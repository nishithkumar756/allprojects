from django.urls import path
from . import views
app_name='proj'
urlpatterns = [
    path('register',views.register_package,name='register'),
    path('login', views.login_view, name='login'),
    path('packages', views.package_list_view, name='list_packages'),
    path('modifypackage/<int:id>', views.modify_package, name='modify'),
    path('package/<int:id>', views.package_detail, name='package_detail'),
    path('logout', views.logout_view, name='logout'),



]