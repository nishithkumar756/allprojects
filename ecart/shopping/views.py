from django.shortcuts import render,redirect
from .models import product_data,customer,cart
from django.contrib.auth import authenticate

# Create your views here.
def home(request):
    return render(request,'shopping/home.html')
def product_data_(request):
    if request.session.get('acccess'):
        product_details =product_data.objects.all()
        context = {'product':product_details}
        return render(request,'shopping/home.html',context)
    return redirect('shopping:login')

def add_to_cart(request,name):
    full_name_ =request.session.get('full_name')
    user_object = customer.objects.get(full_name =full_name_)
    product_data_object =product_data.objects.get(name= name)
    cart_ = [c.product_id.name for c in cart.objects.filter(customer = user_object)]
    if name in cart_:
        cart_ =cart.objects.filter(customer =user_object)
        for car in cart_:
            if car.product_id == product_data_object:
                car.quantity = car.quantity + 1
                car.save()
            else:
                cart_ = cart(customer=user_object,product_id=product_data_object)
                cart_.save()
                return redirect('shopping:product_data')
def cart_data(request):
    full_name_ = request.session.get('full_name')
    user_object =customer.objects.get(full_name = full_name_)
    cart_data = cart.objects.filter(customer =user_object)
    cart_total = sum([c.product_id.price * c.quantity for c in cart.objects.filter()])
    context ={'products':cart_data,'cart_total':cart_total}
    return render(request,'shopping/cart.html',context)

def login(request):
    if request.method == 'POST':
        username_ = request.POST.get('username')
        password_ = request.POST.get('password')
        user = authenticate(username=username_,password=password_)

        user_objects =customer.objects.get(user =user)
        if user is not None:
            request.session['full_name'] = user_objects.full_name
            request.session['access'] = True
            return redirect('shopping:product_data')
    return render(request,'shopping/login.html')

def logout(request):
    del request.sessions['full_name']
    del request.sessions['access']
    return redirect('shopping:login')
