from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import Student, Marks
# Create your views here.

def student_detail(request, id):
    #this is the R of CRUD
    student_ = Student.objects.get(pk=id)
    #SELECT * FROM STUDENT WHERE <INSERT PRIMARY KEY HERE>=id
    marks = Marks.objects.filter(student=student_)
    #SELECT * FROM MARKS WHERE STUDENT=(SELECT * FROM STUDENT
    # WHERE <INSERT PRIMARY KEY HERE>=id)

    context = {'student': student_, 'marks': marks}
    return render(request, 'gradebook/gradebook.html', context)

def add_marks(request, id):
    #this is the C of CRUD
    student_ = Student.objects.get(pk=id)

    sub_name_ = request.POST.get('sub_name')
    score_ = request.POST.get('score')

    marks = Marks(student=student_, subject_name=sub_name_, score=score_)
    marks.save()
    return redirect('gradebook:student_detail', student_.id)

def delete_marks(request, id):
    #this is the D of crud
    marks = Marks.objects.get(pk=id)
    marks.delete()
    return redirect('gradebook:student_detail', marks.student.id)

def update_marks(request,id):
    student_=Student.objects.get(pk=id)
    marks = Marks.objects.filter(student=student_)
    name = request.POST.get('name')
    update_name = request.POST.get('sub_name')
    update_score = request.POST.get('score')
    for item in marks:
        if item.subject_name==name:
           item.subject_name=update_name
           item.score=update_score
           item.save()
        item.save()   
    return redirect('gradebook:student_detail', student_.id)



