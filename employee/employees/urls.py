from django.urls import path,include
from . import views

app_name = "employees"
urlpatterns =[
    path('<int:id>',views.employee_details, name='employee_details'),
    path('employees/<int:id>',views.mark_attendance, name='mark_attendance'),
]