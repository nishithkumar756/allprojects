from django.shortcuts import render,redirect
from django.http import HttpResponse
from . models import Employee,Attendance

# def show_details(request,id):
#     #this is the R of CRUD
#     employee_ = Employees.objects.get(pk=id)
#     #SELECT * FROM STUDENT WHERE <INSERT PRIMARY KEY HERE>=id
#     attendance = Attendance.objects.filter(employee=employee_)
#     #SELECT * FROM MARKS WHERE STUDENT=(SELECT * FROM STUDENT
#     # WHERE <INSERT PRIMARY KEY HERE>=id)

#     context = {'employee': employee_, 'attendance': attendance}
#     return render(request, 'company/details.html', context)




# Create your views here.
def employee_details(request,id):
    employee_ = Employee.objects.get(id=id)
    attendance=Attendance.objects.filter(employee=employee_)

    context ={'employee':employee_,'attendance':attendance}
    return render(request ,'employees/details.html',context)

def mark_attendance(request,id):
    employee_ = Employee.objects.get(id=id)
    date_ = request.POST.get('date')
    attendance_ =request.POST.get('attendance')
    log =Attendance(employee=employee_,date=date_,attendance=attendance_)
    log.save()
    return redirect('employees:employee_details',employee_.id)