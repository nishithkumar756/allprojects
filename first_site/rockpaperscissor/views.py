from django.shortcuts import render



def rock_paper_scissor_function(request):
    if request.method == 'POST':
        player1_choice = request.POST.get('player1_choice')
        player2_choice = request.POST.get('player2_choice')
        result = ''
        if player1_choice== player2_choice:
            result = "It's a tie!"
        elif player1_choice == 'rock':
            if player2_choice == 'paper':
                result = 'Player 2 wins!'
            else:
                result = 'Player 1 wins!'
        elif player1_choice == 'paper':
            if player2_choice == 'scissors':
                result = 'Player 2 wins!'
            else:
                result = 'Player 1 wins!'
        else:
            if player2_choice == 'rock':
                result = 'Player 2 wins!'
            else:
                result = 'Player 1 wins!'
        context={'result':result}
        return render(request, 'rockpaperscissor/rockpaperscissor.html',context)
    else:
        return render(request, 'rockpaperscissor/rockpaperscissor.html',)
# Create your views here.
