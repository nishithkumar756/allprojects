from django.shortcuts import render,redirect
from .models import Package

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

# Create your views here.

def register_package(request):
    if request.method=='GET':
        return render(request,'proj/home.html')
    
    if request.method=='POST':
        address_=request.POST.get('address','')
        phone_=request.POST.get('phone','')

        package=Package(address=address_ , phone=phone_)
        package.save()
        return render(request, 'proj/home.html')
    

def login_view(request):
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        password_ = request.POST.get('password', '')
        user = authenticate(request, username=username_, password=password_)
        if user is not None:
            login(request, user)
            return redirect('proj:list_packages')
    return render(request,'proj/login.html')


@login_required(login_url='proj:login')
def package_list_view(request):
    packages = Package.objects.all()
    return render(request, 'proj/list_package.html', {'packages': packages})




@login_required(login_url='proj:login')
def modify_package(request, id):
    package = Package.objects.get(id=id)
    newstatus = request.POST.get('status', package.status)
    neweta = request.POST.get('eta', package.eta)
    package.status = newstatus
    package.eta = neweta
    package.save()
    return redirect('proj:list_packages')



def package_detail(request, id):
    package = Package.objects.get(id=id)
    return render(request, 'proj/package_detail.html', {'package': package})

def logout_view(request):
    logout(request)
    return redirect('proj:login')

