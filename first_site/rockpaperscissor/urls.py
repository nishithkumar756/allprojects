from django.urls import path
from . import views

urlpatterns=[
    path("game/",views.rock_paper_scissor_function,name="rock_paper_scissor_function")
]