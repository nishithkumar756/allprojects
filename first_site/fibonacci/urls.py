from django.urls import path
from . import views
urlpatterns = [
    path('<int:start>', views.Fibanocii_sequence, name='Fibanocii_sequence'),
    path('withparams', views.get_fibonacii_req_params, name='Fibonocii_withparams'),
    path('processform', views.process_fibonacci_form, name='process_fibonacci_form'),
    path('form', views.show_fibonacci_form, name='fibonacci_collatz_form'),

]
