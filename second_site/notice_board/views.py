
from django.shortcuts import render, redirect
from .models import Notice

def create_notice(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        contact = request.POST.get('contact')
        content = request.POST.get('content')
        Notice.objects.create(title=title, contact=contact, content=content)
        return redirect('notice_list')
    return render(request, 'notice_board/notice_board.html')
    

def notice_list(request):
    notices = Notice.objects.all().order_by('-created_at')[:6]
    return render(request, 'notice_list.html', {'notices':notices })
