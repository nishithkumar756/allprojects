from django.db import models

class Attendance(models.Model):
    employee_id = models.IntegerField()
    date = models.DateField()
    status = models.CharField(max_length=10)
