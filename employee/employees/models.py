

from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=256)

    def _str_(self):
        return f"{self.name}"
    
class Attendance(models.Model):
    date = models.DateField()
    attendance = models.CharField(max_length=256)
    employee = models.ForeignKey(Employee,on_delete=models.CASCADE)

    def str(self):
        return f"{self.employee} - {self.date} - {self.attendance}"
