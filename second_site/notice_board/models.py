from django.db import models

class Notice(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    contact = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

# Create your models here.

