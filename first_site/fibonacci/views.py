from django.shortcuts import render
from django.http import HttpResponse
from.fibonacci import fibonacci

def Fibanocii_sequence(request,start):
    sequence = fibonacci(start)
    standard_response = f'''
    <!DOCTYPE html>
        <html>

            <head>
                <title>Fibonacii</title>
            </head>

            <body>
                <h2>Fibonacci sequence for {start} </h2>
                <p>{sequence}</p>
            </body>

        </html>
        '''

    return HttpResponse(standard_response)

def get_fibonacii_req_params(request):
    start = int(request.GET.get('start', '70'))
    sequence = fibonacci(start)
    standard_response = f'''
    <!DOCTYPE html>
        <html>

            <head>
                <title>Fibonacci</title>
            </head>

            <body>
                <h2>Fibonacci sequence for {start} </h2>
                <p>{sequence}</p>
            </body>

        </html>
        '''

    return HttpResponse(standard_response)

def process_fibonacci_form(request):
    start = int(request.POST.get('start', '1'))
    sequence = fibonacci(start)
    standard_response = f'''
    <!DOCTYPE html>
        <html>

            <head>
                <title>Fibonacci</title>
            </head>

            <body>
                <h2>Fibonacci sequence for {start} </h2>
                <p>{sequence}</p>
            </body>

        </html>
        '''

    return HttpResponse(standard_response)

def show_fibonacci_form(request):
    return render(request, 'fibonacci/fibonacci.html')
