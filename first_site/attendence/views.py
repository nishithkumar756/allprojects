from django.shortcuts import render
from attendence.models import Attendance

def employee_attendance_history(request, employee_id):
    attendance_history = Attendance.objects.filter(employee_id=employee_id)
    return render(request, 'attendance/employee_attendance_history.html', {'attendance_history': attendance_history})


# Create your views here.